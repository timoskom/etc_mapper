import sys
import openpyxl as opxl
import json
import csv
import re

# selected imports
from enum import Enum

replace_regex = re.compile("^\$\(.+\)$")
list_regex = re.compile("^@\([\w\d\s]+\)\[\d+\]$")

# Program config
class AppConfig:

  def __init__(self):
    self.name = "etc_mapper"
    # Major - Minor - Patch [- Build]
    self.long_version = [1, 0, 0, 0]
    # Major - Minor
    self.short_version = [self.long_version[0], self.long_version[1]]
    pass # def

  def get_name(self) -> str:
    return self.name
    pass # def

  def get_version(self, _long:bool = True) -> list:
    if _long:
      return self.long_version
    else:
      return self.short_version
    pass # def

  def print_config(self, _long:bool):
    if _long:
      print(self.name + " v" +
        self.short_version[0].__str__() + "." +
        self.short_version[1].__str__()
      )
    else:
      if self.long_version[3] == 0:
        print(self.name + " v" +
          self.long_version[0].__str__() + "." +
          self.long_version[1].__str__() + "." +
          self.long_version[2].__str__()
        )
      else:
        print(self.name + " v" +
          self.long_version[0].__str__() + "." +
          self.long_version[1].__str__() + "." +
          self.long_version[2].__str__() + "." +
          self.long_version[3].__str__()
        )
    pass # def

  pass # class

# CSV Delimeter
class CSVDelimeter(Enum):
  COMMA = ','
  SEMICOLON = ';'
  pass # class

# CSV Line Terminator
class CSVLineTerminator(Enum):
  CR = '\r'
  LF = '\n'
  CRLF = '\r\n'
  pass # class

# Parsing erros
class ParseError(Enum):
  IN_FILE_ERROR = -2
  OUT_FILE_ERROR = -1
  OK = 0
  GENERIC_ERROR = 1
  pass # class

class Mapping:

  def __init__(self, _config: str):
    self.config = dict()
    with open(file=_config, encoding="utf_8") as config_file:
      conf = json.load(config_file)
      for key, value in conf.items():
        if isinstance(value, list):
          i = 0
          for v in value:
            self.config["@({})[{}]".format(key, i)] = v
            i += 1
            pass # for
          pass # if
        else:
          self.config[key] = value
          pass # else
        pass # for
      pass # with
    pass # def

  def get_mapping(self) -> dict:
    return self.config
    pass # def

  pass # class

class Parser:

  def __init__(self, _input: str, _worksheet: str, _mapping: Mapping):
    self.input = _input
    self.worksheet = _worksheet
    self.mapping = _mapping
    self.result = dict()
    pass # def

  # getter
  def get_input(self) -> str:
    return self.input
    pass # def

  def parse(self) -> ParseError:
    _in = None
    _out = None

    wbook = opxl.load_workbook(filename=self.input, read_only=False)

    wsheet = wbook[self.worksheet]

    src_mapping = self.mapping.get_mapping()
    
    for col in wsheet.columns:
      if col[0].value is None:
        continue
        pass # if
      elif col[0].value in src_mapping.values():
        self.result[col[0].value] = list()
        for c in col[1:]:
          if c != None:
            self.result[col[0].value].append(c.value)
        pass # elif
      pass # for

    wbook.close()
    pass # def

  def get_result(self) -> dict:
    return self.result
    pass # def

  pass # class

class Writer:

  def __init__(self, _output: str, _mapping: Mapping, _parsed: dict):
    self.ouput = _output
    self.mapping = _mapping
    self.parsed = _parsed
    pass # def

  # getter
  def get_output(self) -> str:
    return self.output
    pass # def

  def write(self, _replace_null = None, _csv_delimeter = CSVDelimeter.COMMA, _csv_lineterminator = CSVLineTerminator.LF):
    # newline argument must be set to '', in order to not translate '\n' to OS specific value
    with open(self.ouput, mode='w', encoding="utf_8", newline='') as csv_file:
      m = self.mapping.get_mapping()
      csv_fields = list()
      _b = list()
      for k in m.keys():
        if list_regex.match(k):
          real = k[2:k.index('[') - 1]
          if real not in _b:
            _b.append(real)
            csv_fields.append(real)
            pass # if
          pass # if
        else:
          csv_fields.append(k)
          pass # else
      csv_writer = csv.DictWriter(csv_file, fieldnames=csv_fields, delimiter=_csv_delimeter.value, lineterminator=_csv_lineterminator.value)
      csv_writer.writeheader()

      d = dict()
      i = 0
      _max = 0

      for key, value in self.parsed.items():
        _max = len(value)
        pass # for
      
      counter = 0
      while i < _max:
        for key in m:
          value = m[key]
          #print(key)
          #print(value)
          if value is None:
            d[key] = _replace_null
            continue
            pass # if
          elif replace_regex.match(value):
            d[key] = value[2:len(value) - 1]
            continue
            pass # elif
          elif list_regex.match(key):
            try:
              d[key[2:key.index('[') - 1]]
              pass # try
            except:
              d[key[2:key.index('[') - 1]] = None
              pass # except
            if not d[key[2:key.index('[') - 1]] or int(key[key.index('[') + 1:key.index(']')]) - counter < 0:
              counter = 0
              d[key[2:key.index('[') - 1]] = self.parsed[m[key]][i]
              pass # if
            else:
              counter += 1
              d[key[2:key.index('[') - 1]] += "||" + self.parsed[m[key]][i]
              pass # else
            continue
            pass # elif
          #print(self.parsed[m[key]][i])
          d[key] = self.parsed[m[key]][i]
        csv_writer.writerow(d)
        i = i + 1
      
      del _max
      del i
      del d
      pass # with
    pass # def

  pass # class

def handle_parse_result(_err:ParseError):
  if _err == ParseError.IN_FILE_ERROR:
    print("Could not open input file! Please, check that the file exists and that it isn't used by another program!")
  elif _err == ParseError.OUT_FILE_ERROR:
    print("Could not access output file! Please, try again!")
  else:
    return
  sys.exit(_err)
  pass # def

def handle_help_arg():
  config = AppConfig()

  print("-----HELP-----")
  print("App Name: " + config.get_name())

  # section: info
  print("----INFO----")

  print('''
  The program can map excel sheet columns into csv columns.
  The first row of the sheet should be a label/title row.
  In order to know which excel columns will map into which
  csv columns the program reads a supplied (or the default)
  .mapping.json (config) file. The "config" has the format:

  { 
    "CSVColumn1":"ExcelColumn1",
    "CSVColumn2":"ExcelColumn2",
    "CSVColumn3":"ExcelColumn3",
  }

  With this format the program know which columns to read
  from the excel and into which columns to write them in
  the csv.

  The program, also, takes an input xlsx or xls argument which
  is the name of the excel from which to read and an output
  csv argument which is the name of the csv into which it will
  write the data.

  The excel mappings in the "config" file can also be a list
  in which case multiple excel columns are combined into one
  with each value seperated by two vertical bars (||). The
  .mapping.json file can look like this:

  {
    "CSVColumn1":"ExcelColumn1"
    "CSVColumn1":[
      "ExcelColumn2",
      "ExcelColumn4"
    ],
    "CSVColumn3":"ExcelColumn3"
  }

  Also, there is the "dollar" pseudo-function which uses its
  argument as a literal on every entry in the csv. For instance
  a mapping like this:

  {
    ...
    "CSVColumnN":"$(0.0)",
    ...
  }

  will put the literal "0.0" on every entry of the CSVColumnN
  in the csv file. As an example the excel and mapping.json
  that are shown bellow will produce the csv that is also shown
  bellow.


  [excel]

  +----+----+
  | C1 | C2 |
  +----+----+
  |  1 |  a |
  +----+----+
  |  2 |  b |
  +----+----+
  |  3 |  c |
  +----+----+


  [mapping.json]

  {
    "CSV1":"C1",
    "CSV2":"$(True)",
    "CSV3":"C2"
  }


  [csv]

  CSV1,CSV2,CSV3
  1,True,a
  2,True,b
  3,True,c

  ''')

  print("----INFO----")

  # section: supported arguments
  print("----SUPPORTED ARGUMENTS----")

  # arg: help
  print("--help--")
  print("\tFormats: {}".format(["--help", "-h"]))
  print("\tArguments: {}".format(None))
  print("\tInfo: The 'help' argument prints info about the program and then exits. A must is that should be the only argument, else the program crashes")
  print("--help--")

  # arg: input
  print("--input--")
  print("\tFormats: {}".format(["--in", "--input", "-i"]))
  print("\tArguments: The name of the input file")
  print("\tInfo: The 'input' argument specifies the input (excel) file that the program will read from")
  print("--input--")

  # arg: output
  print("--output--")
  print("\tFormats: {}".format(["--out", "--output", "-o"]))
  print("\tArguments: The name of the output file")
  print("\tInfo: The 'output' argument specifies the output (csv) file that the program will write to")
  print("--output--")

  # arg: worksheet
  print("--worksheet--")
  print("\tFormats: {}".format(["--worksheet", "-w"]))
  print("\tArguments: The name of the sheet")
  print("\tInfo: The 'worksheet' argument specifies the sheet name from which the program will read from")
  print("--worksheet--")

  # arg: config
  print("--config--")
  print("\tFormats: {}".format(["--config", "-c"]))
  print("\tArguments: The name of the config (mapping) file")
  print("\tInfo: The 'config' argument specifies the name of the config (mapping) file")
  print("--config--")

  # arg: replace null
  print("--replace null--")
  print("\tFormats: {}".format(["--replace-null", "-n"]))
  print("\tArguments: The replacement value")
  print("\tInfo: The 'replace null' argument specifies the value with which the program should replace the null mappings")
  print("--replace null--")

  # arg: csv delimeter
  print("--csv delimeter--")
  print("\tFormats: {}".format(["--csv-delimeter", "--csv-seperator", "-d"]))
  print("\tArguments: The csv delimeter (one of: {})".format([CSVDelimeter.COMMA.name, CSVDelimeter.SEMICOLON.name]))
  print("\tInfo: The 'csv delimeter' argument specifies the delimeter that should be used in the csv file")
  print("--csv delimeter--")

  # arg: csv lineterminator
  print("--csv lineterminator--")
  print("\tFormats: {}".format(["--csv-lineterminator", "-t"]))
  print("\tArguments: The csv terminator (one of: {})".format([CSVLineTerminator.CR.name, CSVLineTerminator.LF.name, CSVLineTerminator.CRLF.name]))
  print("\tInfo: The 'csv lineterminator' argument specifies the line terminator character to be used in the csv file")
  print("--csv lineterminator--")

  print("----SUPPORTED ARGUMENTS----")

  print("-----HELP-----")

  del config
  sys.exit(0)
  pass # def

def parse_args() -> list:
  # Internal enum
  class Argument(Enum):
    ARG_NONE = 0
    ARG_INPUT = 1
    ARG_OUTPUT = 2
    ARG_WORKSHEET = 3
    ARG_CONFIG = 4
    ARG_REPLACE_NULL = 5
    ARG_CSV_DELIMETER = 6
    ARG_CSV_LINETERMINATOR = 7
    pass # class

  if sys.argv[1] in ["--help", "-h"]:
    if len(sys.argv) == 2:
      handle_help_arg()
      pass # if
    else:
      print("Argument '" + sys.argv[1] + "' should be the only argument!!!")
      sys.exit(-1)
      pass # if
    pass # if

  # Default values if no arguments are supplied
  result = ["in_default.xlsx", "out_default.csv", "Sheet1", "default.mapping.json", None, CSVDelimeter.COMMA, CSVLineTerminator.CR]

  nxt = Argument.ARG_NONE
  for arg in sys.argv[1 : len(sys.argv)]: # skip first arg (it's the script)
    if nxt == Argument.ARG_INPUT: # input argument (skip to next iteration)
      result[0] = arg
      nxt = Argument.ARG_NONE
      continue
    elif nxt == Argument.ARG_OUTPUT: # output argument (skip to next iteration)
      result[1] = arg
      nxt = Argument.ARG_NONE
      continue
    elif nxt == Argument.ARG_WORKSHEET: # worksheet argument (skip to next iteration)
      result[2] = arg
      nxt = Argument.ARG_NONE
      continue
    elif nxt == Argument.ARG_CONFIG: # config argument (skip to next iteration)
      result[3] = arg
      nxt = Argument.ARG_NONE
      continue
    elif nxt == Argument.ARG_REPLACE_NULL: # replace null argument (skip to next iteration)
      result[4] = arg
      nxt = Argument.ARG_NONE
      continue
    elif nxt == Argument.ARG_CSV_DELIMETER: # csv delimeter argument (skip to next iteration)
      if arg.lower() == CSVDelimeter.COMMA.name.lower():
        result[5] = CSVDelimeter.COMMA
        pass # if
      elif arg.lower() == CSVDelimeter.SEMICOLON.name.lower():
        result[5] = CSVDelimeter.SEMICOLON
        pass # elif
      nxt = Argument.ARG_NONE
      continue
    elif nxt == Argument.ARG_CSV_LINETERMINATOR: # csv lineterminator argument (skip to next iteration)
      if arg.lower() == CSVLineTerminator.CR.name.lower():
        result[6] = CSVLineTerminator.CR
        pass # if
      elif arg.lower() == CSVLineTerminator.LF.name.lower():
        result[6] = CSVLineTerminator.LF
        pass # elif
      elif arg.lower() == CSVLineTerminator.CRLF.name.lower():
        result[6] = CSVLineTerminator.CRLF
        pass # elif
      nxt = Argument.ARG_NONE
      continue

    if arg in ["--in", "--input", "-i"]:
      nxt = Argument.ARG_INPUT
    elif arg in ["--out", "--ouput" , "-o"]:
      nxt = Argument.ARG_OUTPUT
    elif arg in ["--worksheet", "-w"]:
      nxt = Argument.ARG_WORKSHEET
    elif arg in ["--config", "-c"]:
      nxt = Argument.ARG_CONFIG
    elif arg in ["--replace-null", "-n"]:
      nxt = Argument.ARG_REPLACE_NULL
    elif arg in ["--csv-delimeter", "--csv-seperator", "-d"]:
      nxt = Argument.ARG_CSV_DELIMETER
    elif arg in ["--csv-lineterminator", "-t"]:
      nxt = Argument.ARG_CSV_LINETERMINATOR
    else:
      raise Exception(arg, "Unsupported argument")

    pass # for
  
  print(result)
  return result
  pass # def

def main():
  # Internal enum
  class ArgumentError(Enum):
    FALSE_INPUT_FILE_NAME = -1
    FALSE_OUTPUT_FILE_NAME = -2
    FALSE_CONFIG_FILE_NAME = -3
    pass # class

  config = AppConfig()
  config.print_config(False)

  _in, _out, _worksheet, _mapping_config, _replace_null, _csv_delimeter, _csv_lineterminator = parse_args()

  if not (_in.endswith(".xlsx") or _in.endswith(".xls")):
    print("Supplied input file is not an excel document!")
    sys.exit(ArgumentError.FALSE_INPUT_FILE_NAME)
    pass # if

  if not _out.endswith(".csv"):
    print("Supplied output file is not a csv file!")
    sys.exit(ArgumentError.FALSE_OUTPUT_FILE_NAME)
    pass # if

  if not _mapping_config.endswith(".mapping.json"):
    print("Supplied config file is not a mapping config file!")
    sys.exit(ArgumentError.FALSE_CONFIG_FILE_NAME)
    pass # if

  mapping = Mapping(_mapping_config)
  parser = Parser(_in, _worksheet, mapping)
  handle_parse_result(parser.parse())
  result = parser.get_result()
  writer = Writer(_out, mapping, result)
  writer.write(_replace_null = _replace_null, _csv_delimeter = _csv_delimeter, _csv_lineterminator = _csv_lineterminator)

  del writer
  del result
  del parser
  del mapping
  del _csv_lineterminator
  del _csv_delimeter
  del _replace_null
  del _mapping_config
  del _worksheet
  del _out
  del _in
  del config
  pass # def

if __name__ == "__main__":
  main()