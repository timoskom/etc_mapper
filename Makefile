test:
	py main.py -w "Test" -n test

test_help:
	py main.py -h

etc_mapper:
	pyinstaller main.py --onefile --console --name etc_mapper
	copy in_default.xlsx "dist/in_default.xlsx" /b
	copy out_default.csv "dist/out_default.csv"
	copy default.mapping.json "dist/default.mapping.json"

run: etc_mapper
	./dist/$^ -w "Test" -n "Test"